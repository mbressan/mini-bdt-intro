#include <TFile.h>
#include <TTree.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TRandom.h>
#include <TMVA/Factory.h>
#include <TMVA/DataLoader.h>
#include <TMVA/Tools.h>

int main() {
    // Initialize TMVA
    TMVA::Tools::Instance();

    // Create a ROOT file to store the output
    TFile* outputFile = TFile::Open("TMVA_BDT_Example.root", "RECREATE");
    if (!outputFile) {
        std::cerr << "Error: Could not create output file." << std::endl;
        return 1;
    }

    // Create a factory object and a DataLoader
    TMVA::Factory* factory = new TMVA::Factory("TMVAClassification", outputFile,
                                               "!V:!Silent:Color:DrawProgressBar:Transformations=I:AnalysisType=Classification");
    TMVA::DataLoader* dataloader = new TMVA::DataLoader("dataset");

    // Load data (this is a simple example, normally you would read from a TTree)
    // Here we create a simple TTree for demonstration
    TTree* signalTree = new TTree("Signal", "Signal");
    TTree* backgroundTree = new TTree("Background", "Background");

    float variable1, variable2, weight;
    signalTree->Branch("variable1", &variable1, "variable1/F");
    signalTree->Branch("variable2", &variable2, "variable2/F");
    signalTree->Branch("weight", &weight, "weight/F");
    backgroundTree->Branch("variable1", &variable1, "variable1/F");
    backgroundTree->Branch("variable2", &variable2, "variable2/F");
    backgroundTree->Branch("weight", &weight, "weight/F");

    // Fill the signal tree with some random data
    for (int i = 0; i < 1000; ++i) {
        variable1 = gRandom->Gaus(1, 1);
        variable2 = gRandom->Gaus(1, 1);
        weight = 1.0;
        signalTree->Fill();
    }

    // Fill the background tree with some random data
    for (int i = 0; i < 1000; ++i) {
        variable1 = gRandom->Gaus(-1, 1);
        variable2 = gRandom->Gaus(-1, 1);
        weight = 1.0;
        backgroundTree->Fill();
    }

    // Register the trees in the dataloader
    dataloader->AddSignalTree(signalTree, 1.0);
    dataloader->AddBackgroundTree(backgroundTree, 1.0);

    // Add variables
    dataloader->AddVariable("variable1", 'F');
    dataloader->AddVariable("variable2", 'F');

    // Prepare the training and testing dataset
    dataloader->PrepareTrainingAndTestTree("", "",
                                           "nTrain_Signal=500:nTrain_Background=500:nTest_Signal=500:nTest_Background=500:SplitMode=Random:NormMode=NumEvents:!V");

    // Book the BDT method
    factory->BookMethod(dataloader, TMVA::Types::kBDT, "BDT",
                        "!H:!V:NTrees=100:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:SeparationType=GiniIndex:nCuts=20");

    // Train, Test and Evaluate the classifier
    factory->TrainAllMethods();
    factory->TestAllMethods();
    factory->EvaluateAllMethods();

    // Save the output
    outputFile->Close();

    // Clean up
    delete factory;
    delete dataloader;
    delete outputFile;

    return 0;
}


void bdt_example(){
    main();
}