# Mini BDT Intro

This is a very short example of how to run a BDT with TMVA that ChatGPT made for me. This script creates dummy signal and background trees with two variables each and gaussian distributions. More documentation and options in the [TMVA Users Guide](https://root.cern.ch/download/doc/tmva/TMVAUsersGuide.pdf) (section 8.13 for BDTs) and [MethodBDT Documentation](https://root.cern.ch/doc/master/classTMVA_1_1MethodBDT.html). A (rather involved) example can be found [here](https://root.cern/doc/master/TMVAClassification_8C.html) as well.

To run the code, use
```
root -q bdt_example.C
```
and you can take a look at the output file using the TMVA GUI with
```
root -l -e 'TMVA::TMVAGui("TMVA_BDT_Example.root")'
```
